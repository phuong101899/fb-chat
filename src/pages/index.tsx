

import React from "react"

const RandomNumberGenerator = () => {

  const randomNumber = Math.random();

  return (
      <p>Random number: {randomNumber}</p>
  )
}

export default RandomNumberGenerator
